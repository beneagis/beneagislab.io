package stuff.util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.Translate.TranslateOption;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

public class TranslateUtil {
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyMMddHHmmssSSS");

	private static String date() {
		return DATE_FORMAT.format(Calendar.getInstance().getTime());
	}

	private static final String EXECUTION_DATE = date();

	private static Translate service;
	private static CsvUtil csv;
	private static boolean changed;

	private static Map<MultiKey<String>, String> cache;
	private static Map<MultiKey<String>, String> used = new LinkedHashMap<MultiKey<String>, String>();

	private static void initCache() throws IOException {
		cache = new LinkedHashMap<MultiKey<String>, String>();
		File root = new File("bark/cache/translate/");

		File[] files = ArrayUtils.add(
				root.listFiles((dir, name) -> (name.toLowerCase().endsWith(".csv")
						&& !name.equals("force.csv") /* && !name.equals("used.csv") */)),
				new File("bark/cache/translate/force.csv"));

		for (File file : files) {
			CSVParser records = CsvUtil.read(file.getAbsolutePath());
			for (CSVRecord record : records) {
				String source = record.get("source");
				String target = record.get("target");
				String text = record.get("text");
				String translatedText = record.get("translatedText");

				cache.put(new MultiKey<String>(source, target, text), translatedText);
			}
			records.close();
		}
	}

	private static void initCsv() throws IOException {
		csv = new CsvUtil("bark/cache/translate/" + EXECUTION_DATE + ".csv", false, false, "source", "target", "text",
				"translatedText");
	}

	private static void initService() throws IOException {
		service = TranslateOptions.getDefaultInstance().getService();
	}

	private static String get(String source, String target, String text) throws IOException {
		if (cache == null) {
			initCache();
		}
		return cache.get(new MultiKey<String>(source, target, text));
	}

	private static String put(String source, String target, String text, String translatedText) throws IOException {
		changed = true;
		return cache.put(new MultiKey<String>(source, target, text), translatedText);
	}

	private static final String DEFAULT_SOURCE = "en";
	private static final String DEFAULT_TARGET = "pt";
	private static final String DEFAULT_FORMAT = "text";

	public static String tr(String text) throws IOException {
		return tr(DEFAULT_SOURCE, DEFAULT_TARGET, text, DEFAULT_FORMAT);
	}

	public static String tr(String text, String format) throws IOException {
		return tr(DEFAULT_SOURCE, DEFAULT_TARGET, text, format);
	}

	public static String tr(String source, String target, String text, String format) throws IOException {

		String translatedText = "";

		if (StringUtils.isNotBlank(text)) {
			translatedText = get(source, target, text);
			if (StringUtils.isBlank(translatedText)) {

				int retry = 1;
				do {
					try {
						translatedText = service(source, target, text, format);

						if (StringUtils.isNotBlank(translatedText)) {
							break;
						} else {
							retry--;
						}
					} catch (Exception e) {
						e.printStackTrace();
						retry--;
						try {
							TimeUnit.MILLISECONDS.sleep(100000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
					}

				} while (retry > 0);
				put(source, target, text, translatedText);
				addCsv(source, target, text, translatedText);
			}
			used.put(new MultiKey<String>(source, target, text), translatedText);
		}
		return translatedText;
	}

	private static String service(String source, String target, String text, String format) throws IOException {
		if (service == null) {
			initService();
		}

		String translatedText;
		TranslateOption s = TranslateOption.sourceLanguage(source);
		TranslateOption t = TranslateOption.targetLanguage(target);
		TranslateOption f = TranslateOption.format(format);

		Translation translation = service.translate(text, s, t, f);
		translatedText = translation.getTranslatedText();
		System.out.println("< " + text);
		System.out.println("> " + translatedText);
		System.out.println();
		return translatedText;
	}

	public static void addCsv(String source, String target, String text, String translatedText) throws IOException {
		if (csv == null) {
			initCsv();
		}
		csv.add(source, target, text, translatedText);
		csv.flush();
	}

	public static void done(boolean success) throws IOException {
		if (changed) {
			CsvUtil csv = new CsvUtil("bark/cache/translate/" + date() + ".csv", false, false, "source", "target",
					"text", "translatedText");

			for (Entry<MultiKey<String>, String> entry : cache.entrySet()) {
				MultiKey<String> key = entry.getKey();
				csv.add(key.getKey(0), key.getKey(1), key.getKey(2), entry.getValue());
			}
			csv.flush();
		}
		if (success) {
			CsvUtil csv = new CsvUtil("bark/cache/translate/used.csv", false, false, "source", "target", "text",
					"translatedText");

			for (Entry<MultiKey<String>, String> entry : used.entrySet()) {
				MultiKey<String> key = entry.getKey();
				csv.add(key.getKey(0), key.getKey(1), key.getKey(2), entry.getValue());
			}
			csv.flush();
		}
	}
}
