package stuff.util;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;

import com.google.common.collect.Lists;

public class CartesianProduct {

	public static List<?> product(List<?>... a) {
		if (a.length >= 2) {
			List<?> product = a[0];
			for (int i = 1; i < a.length; i++) {
				product = product(product, a[i]);
			}
			return product;
		}

		return emptyList();
	}

	private static <A, B> List<?> product(List<A> a, List<B> b) {
		return of(a.stream().map(e1 -> of(b.stream().map(e2 -> asList(e1, e2)).collect(toList())).orElse(emptyList()))
				.flatMap(List::stream).collect(toList())).orElse(emptyList());
	}

	public static void main(String[] args) {
		List<String> numbers = Arrays.asList("1", "2", "3");
		List<String> letters = Arrays.asList("A", "B", "C");
		List<String> roman = Arrays.asList("i", "v", "x");

		Object product = Lists.cartesianProduct(new List[] { letters, numbers, roman });
		// Object product = product(letters, numbers, roman);

		System.out.println(JsonUtil.toString(product));
	}
}