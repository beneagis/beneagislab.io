package stuff.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FileUtil {
	public static void write(String file, String text) throws IOException {
		new File(file).getParentFile().mkdirs();

		PrintWriter writer = new PrintWriter(new FileWriter(file), true);
		writer.write(text);
		writer.flush();
		writer.close();
	}

	public static String s(String file) {
		return file.replaceAll("[^a-zA-Z0-9-_\\.]+", "_");
	}

	public static void main(String[] args) throws IOException {
		write("content/raw/test.html", "<html/>");
	}
}
