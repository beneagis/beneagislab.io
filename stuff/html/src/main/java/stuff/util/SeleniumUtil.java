package stuff.util;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumUtil {
	public static WebDriver getChromeDriver() {
		System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
		return new ChromeDriver();
	}

	public static String attr(WebElement element, String name) {
		return element.getAttribute(name);
	}

	public static String text(WebElement element) {
		return text(element, null);
	}

	public static String text(SearchContext context, String css) {
		WebElement element;
		if (css == null && context instanceof WebElement) {
			element = (WebElement) context;
		} else {
			element = find1(context, css);
		}
		if (element != null) {
			return element.getText().trim();
		} else {
			return null;
		}
	}

	public static List<WebElement> find(SearchContext context, String css) {
		return context.findElements(By.cssSelector(css));
	}

	public static WebElement find1(SearchContext context, String css) {
		try {
			return context.findElement(By.cssSelector(css));
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public static List<WebElement> xpath(SearchContext context, String xpath) {
		return context.findElements(By.xpath(xpath));
	}

	public static WebElement xpath1(SearchContext context, String xpath) {
		try {
			return context.findElement(By.xpath(xpath));
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public static WebElement click(WebElement element) {
		return click(element, null);
	}

	public static WebElement click(SearchContext context, String css) {
		WebElement element;
		if (css == null && context instanceof WebElement) {
			element = (WebElement) context;
		} else {
			element = find1(context, css);
		}
		if (element != null) {
			element.click();
		}
		return element;
	}

	private static WebElement displayed(SearchContext context, String css) {
		List<WebElement> elements = context.findElements(By.cssSelector(css));
		for (WebElement element : elements) {
			try {
				if (element.isDisplayed()) {
					return element;
				}
			} catch (StaleElementReferenceException e) {
			}
		}
		return null;
	}

	public static void waitNot(final WebDriver driver, final SearchContext context, final String css) {
		(new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				WebElement element = displayed(context, css);
				return element == null;
			}
		});

	}

	public static void wait(final WebDriver driver, final SearchContext context, final String css) {
		(new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				WebElement element = displayed(context, css);
				return !(element == null);
			}
		});
	}

	public static void wait(final WebDriver driver, final SearchContext context, final String css, final String value) {
		(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				try {
					WebElement element = displayed(context, css);
					return element != null && element.getText().contains(value);
				} catch (StaleElementReferenceException e) {
					return false;
				}
			}
		});
	}

	public static void waitNot(final WebDriver driver, final SearchContext context, final String css,
			final String value) {
		(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				try {
					WebElement element = displayed(context, css);
					return !(element != null && element.getText().contains(value));
				} catch (StaleElementReferenceException e) {
					return false;
				}
			}
		});
	}

	public static WebElement type(SearchContext context, String css, String value) {
		WebElement element = displayed(context, css);
		if (element != null) {
			element.sendKeys(value);
		}
		return element;
	}

}
