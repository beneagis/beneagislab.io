package stuff.util;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.StringUtils;

public class CsvUtil {
	PrintWriter writer;
	CSVPrinter printer;
	String[] header;

	public CsvUtil(String file, boolean append, boolean autoFlush, final String... header) throws IOException {
		new File(file).getParentFile().mkdirs();

		this.header = header;
		this.writer = new PrintWriter(new FileWriter(file), autoFlush);
		this.printer = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(header));
	}

	public CsvUtil(String... header) throws IOException {
		this.header = header;
		this.writer = new PrintWriter(System.out, true);
		this.printer = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(header));
	}

	public static CSVParser read(String file) throws IOException {
		Reader reader = new FileReader(file);
		CSVParser parser = new CSVParser(reader,
				CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim().withCommentMarker('#'));
		return parser;
	}

	public void add(Object... values) throws IOException {
		printer.printRecord(Arrays.copyOf(values, header.length));
	}

	public void add(List<?> record) throws IOException {
		printer.printRecord(record.subList(0, Math.min(record.size(), header.length - 1)));
	}

	public void flush() throws IOException {
		printer.flush();
	}

	// public static void main(String[] args) throws IOException {
	// CsvUtil util = new CsvUtil("Student Name", "Fees");
	// util.add("Dev Bhatia", 4000);
	// util.add("Dev Bhatia", 4000);
	// util.add("Dev Bhatia", 4000);
	// util.flush();
	//
	// }

	public static String t(String text) {
		return text.replace('–', '-').replace("’", "'").replace('“', '\"').replace('”', '\"')
				.replaceAll("([\n\t ]|&nbsp;)+", " ").replaceAll("^(<br>|[ ])+", "").replaceAll("(<br>|[ ])+$", "").trim();
	}

	public static String t(Number n) {
		return n.doubleValue() == 0 ? "" : String.valueOf(n);
	}

	public static String t(Object o) {
		if (o == null)
			return "";
		else if (o instanceof Number) {
			return t((Number) o);
		}
		return t(String.valueOf(o));
	}

	public static String h(String text) {
		if (text == null) {
			return h(0);
		} else {
			return h(text.hashCode());
		}
	}

	public static String i(Object... values) {
		// List<String> i = new ArrayList<String>();
		// for (Object o : values) {
		// String t = t(o);
		// if (StringUtils.isNotBlank(t)) {
		// i.add(t);
		// }
		// }
		return StringUtils.join(values, ".");
	}

	private static String h(int n) {
		String result = (n < 0 ? "" : "+") + Integer.toString(n, 16);
		return result;
	}

	public static void main(String[] args) {
		System.out.println(h(+10));
		System.out.println(h(-10));
	}
}
