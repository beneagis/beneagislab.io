package stuff.download.bark;

import static stuff.util.SeleniumUtil.attr;
import static stuff.util.SeleniumUtil.click;
import static stuff.util.SeleniumUtil.find;
import static stuff.util.SeleniumUtil.find1;
import static stuff.util.SeleniumUtil.getChromeDriver;
import static stuff.util.SeleniumUtil.text;
import static stuff.util.SeleniumUtil.type;
import static stuff.util.SeleniumUtil.waitNot;
import static stuff.util.SeleniumUtil.xpath1;
import static stuff.util.CsvUtil.t;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import stuff.download.bark.model.Option;
import stuff.download.bark.model.Question;
import stuff.util.CsvUtil;
import stuff.util.FileUtil;
import stuff.util.SeleniumUtil;

public class BarkDownload {

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyMMddHHmmssSSS");

	public static String date() {
		return DATE_FORMAT.format(Calendar.getInstance().getTime());
	}

	private static final String EXECUTION_DATE = date();

	public static void main(String[] args) throws InterruptedException, IOException {
		WebDriver driver = getChromeDriver();

		List<Question> questionLog = new ArrayList<Question>();
		Map<String, Map<String, Set<String>>> responseLog = new LinkedHashMap<String, Map<String, Set<String>>>();

		try {
			int total = BarkConstants.scenarios().size();
			int progress = 0;

			Map<String, Integer> optionRotationMap = new LinkedHashMap<String, Integer>();
			driver.get("https://bark-o-matic.com/questions/");

			for (Entry<String, Map<String, Set<String>>> scenario : BarkConstants.scenarios().entrySet()) {
				String scenarioKey = scenario.getKey();
				progress(++progress, total, scenarioKey);

				Map<String, Set<String>> scenarioQuestions = scenario.getValue();
				driver.get("https://bark-o-matic.com/questions/");

				while (driver.getCurrentUrl().startsWith("https://bark-o-matic.com/questions/")) {

					Question question = new Question(driver.getCurrentUrl(), text(driver, "h2"), text(driver, "p"));
					int index = questionLog.indexOf(question);
					if (index >= 0) {
						question = questionLog.get(index);
					} else {
						questionLog.add(question);
					}

					validateId(question);

					List<WebElement> optionList = find(driver, ".option-container");

					for (WebElement element : optionList) {
						WebElement input = find1(element, "input");
						String optionId = attr(input, "value");
						String optionPosition = attr(input, "id");
						String optionType = attr(input, "type");
						String optionName = attr(input, "name");

						WebElement label = find1(element, "label");
						String optionLabelText = text(label);
						String optionLabelPosition = attr(label, "for");

						Option option = question.addOption(optionId, optionPosition, optionLabelText, optionType,
								optionName);

						validateId(option);
						validatePosition(option, optionPosition, optionLabelPosition);
					}

					Set<String> scenarioOptions = scenarioQuestions.get(question.getId());
					if (scenarioOptions == null || scenarioOptions.isEmpty()) {
						Integer p = optionRotationMap.get(question.getId());
						if (p == null || p >= optionList.size()) {
							p = 0;
						}
						WebElement selected = optionList.get(p);

						WebElement label = find1(selected, "label");
						String optionLabelText = text(label);

						if ("No".equalsIgnoreCase(optionLabelText) || "None".equalsIgnoreCase(optionLabelText)
								|| "Not Listed".equalsIgnoreCase(optionLabelText)
								|| "Not Applicable".equalsIgnoreCase(optionLabelText)) {
							p = 0;
							selected = optionList.get(p);
						}
						optionRotationMap.put(question.getId(), ++p);

						WebElement clicked = click(selected);
						WebElement input = find1(clicked, "input");
						String optionId = attr(input, "value");
						logResponse(responseLog, scenarioKey, question.getId(), optionId, true);
					} else {
						for (String optionId : scenarioOptions) {
							WebElement element = find1(driver, "input[value='" + optionId + "']");
							click(xpath1(element, ".."));
							logResponse(responseLog, scenarioKey, question.getId(), optionId, false);
						}
					}

					if (click(driver, "input[name='commit'][value='Next']") == null) {
						break;
					}
					TimeUnit.MILLISECONDS.sleep(1000);
					waitNot(driver, driver, "input[name='commit'][value='Saving...']");
					TimeUnit.MILLISECONDS.sleep(2000);
				}

				validateEmailPage(driver);
				type(driver, "input[id='submission_email']", "jane@example.com");
				click(driver, "input[id='submission_email_updates']");
				click(driver, "input[name='commit'][value='Show me my results']");

				SeleniumUtil.wait(driver, driver, "section.recommendations");
				TimeUnit.MILLISECONDS.sleep(1000);

				logResponse(responseLog, scenarioKey, "", StringUtils.substringAfterLast(driver.getCurrentUrl(), "/"),
						false);

				String contentHtml = attr(find1(driver, "section.recommendations"), "outerHTML");
				FileUtil.write("bark/html/raw/" + scenarioKey + ".html", contentHtml);
			}
		} finally {
			{
				System.out.println("---------------");
				CsvUtil questionlog = new CsvUtil("bark/log/questionlog-" + EXECUTION_DATE + ".csv", false, false, //
						"question.title", // 1
						// "question.description", // 2
						"question.id", // 3
						// "question.url", // 4
						"option.text", // 5
						"option.id", // 6
						// "option.position", // 7
						// "option.name", // 8
						"option.type"// 9
				);
				for (Question question : questionLog) {
					questionlog.add(//
							t(question.getTitle()), // 1
							// question.getDescription(), // 2
							question.getId() // 3
					// question.getUrl()// 4
					);
					for (Option option : question.getOptionList()) {
						questionlog.add(//
								t(question.getTitle()), // 1
								// question.getDescription(), // 2
								question.getId(), // 3
								// question.getUrl(), // 4
								t(option.getText()), // 5
								option.getId(), // 6
								// option.getPosition(), // 7
								// option.getName(), // 8
								option.getType()// 9
						);
					}
				}
				questionlog.flush();
			}
			{
				System.out.println("---------------");
				CsvUtil responselog = new CsvUtil("bark/log/responselog-" + EXECUTION_DATE + ".csv", false, false, //
						"scenario", // 1
						"generated", // 2
						"question.id", // 3
						"option.id" // 4
				);

				for (Entry<String, Map<String, Set<String>>> response : responseLog.entrySet()) {
					for (Entry<String, Set<String>> question : response.getValue().entrySet()) {
						for (String option : question.getValue()) {
							String generated = "pre";
							if (option.endsWith("*")) {
								generated = "gen";
								option = option.replace("*", "");
							}
							responselog.add(//
									response.getKey(), // 1
									generated, // 2
									question.getKey(), // 3
									option // 4
							);

						}
					}
				}
				responselog.flush();
			}
		}
	}

	private static void logResponse(Map<String, Map<String, Set<String>>> responseLog, String scenarioKey,
			String questionId, String optionId, boolean generated) {
		Map<String, Set<String>> response = responseLog.get(scenarioKey);
		if (response == null) {
			response = new LinkedHashMap<String, Set<String>>();
			responseLog.put(scenarioKey, response);
		}
		Set<String> options = response.get(questionId);
		if (options == null) {
			options = new LinkedHashSet<String>();
			response.put(questionId, options);
		}
		options.add(optionId + (generated ? "*" : ""));

	}

	private static void progress(int progress, int total, String stage) {
		System.out.println(date() + " " + progress + "/" + total + ": " + stage);
	}

	private static void error(String message) {
		// RuntimeException e = new IllegalStateException(message);
		// e.printStackTrace();
		// throw e;
		System.err.println("ERROR: " + message);
	}

	private static void validateEmailPage(WebDriver driver) {
		if (!"https://bark-o-matic.com/email".equals(driver.getCurrentUrl())) {
			error("Not in email page " + driver.getCurrentUrl());
		}
	}

	private static void validateId(Question question) {
		Set<String> options = BarkConstants.questions().get(question.getId());
		if (options == null) {
			error("No question found for " + question.getId());
		}
	}

	private static void validateId(Option option) {
		Set<String> options = BarkConstants.questions().get(option.getQuestionId());
		if (options == null) {
			error("No question found for " + option.getQuestionId() + "-" + option.getId());
		}
		if (options == null || !options.contains(option.getId())) {
			error("No option match for " + option.getQuestionId() + "-" + option.getId());
		}
	}

	public static void validatePosition(Object object, String expected, String actual) {
		if (!StringUtils.equalsIgnoreCase(expected, actual)) {
			error("No position match for " + object);
		}
	}

}
