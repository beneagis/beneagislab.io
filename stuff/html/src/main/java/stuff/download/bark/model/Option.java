package stuff.download.bark.model;

import stuff.util.JsonUtil;

public class Option implements Comparable<Object> {
	private String id;
	private String position;
	private String text;
	private String type;
	private String name;
	private String questionId;

	public Option() {
	}

	public Option(String id, String position, String text, String type, String name, String questionId) {
		super();
		this.id = id;
		this.position = position;
		this.text = text;
		this.type = type;
		this.name = name;
		this.questionId = questionId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String toString() {
		return JsonUtil.toString(this);
	}

	@Override
	public int hashCode() {
		if (id == null) {
			return 0;
		}
		return id.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		String otherId = null;
		if (o instanceof String) {
			otherId = (String) o;
		} else if (o instanceof Option) {
			Option other = (Option) o;
			otherId = other.getId();
		}

		if (id == otherId) {
			return true;
		} else if (id == null) {
			return false;
		} else if (otherId == null) {
			return false;
		}

		return id.equals(otherId);
	}

	@Override
	public int compareTo(Object o) {
		String otherId = null;
		if (o instanceof String) {
			otherId = (String) o;
		} else if (o instanceof Option) {
			Option other = (Option) o;
			otherId = other.getId();
		}

		if (id == null && otherId == null) {
			return 0;
		} else if (id == null) {
			return -1;
		} else if (otherId == null) {
			return +1;
		}
		return id.compareTo(otherId);
	}
}
