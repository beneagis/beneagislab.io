package stuff.download.bark.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import stuff.util.JsonUtil;

public class Question implements Comparable<Object> {

	private String url;
	private String id;
	private String title;
	private String description;

	private List<Option> optionList;

	public Question() {
		this.optionList = new ArrayList<Option>();
	}

	public Question(String url, String title, String description) {
		super();
		this.optionList = new ArrayList<Option>();

		this.url = url;
		this.title = title;
		this.description = description;

		if (StringUtils.isNotBlank(url)) {
			this.id = StringUtils.substringAfterLast(url, "/");
		}
	}

	public Option addOption(String id, String position, String text, String type, String name) {
		Option item = new Option(id, position, text, type, name, this.id);
		int index = optionList.indexOf(item);
		if (index >= 0) {
			item = optionList.get(index);
		} else {
			optionList.add(item);
		}
		return item;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Option> getOptionList() {
		return optionList;
	}

	public String toString() {
		return JsonUtil.toString(this);
	}

	@Override
	public int hashCode() {
		if (id == null) {
			return 0;
		}
		return id.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		String otherId = null;
		if (o instanceof String) {
			otherId = (String) o;
		} else if (o instanceof Question) {
			Question other = (Question) o;
			otherId = other.getId();
		}

		if (id == otherId) {
			return true;
		} else if (id == null) {
			return false;
		} else if (otherId == null) {
			return false;
		}

		return id.equals(otherId);
	}

	@Override
	public int compareTo(Object o) {
		String otherId = null;
		if (o instanceof String) {
			otherId = (String) o;
		} else if (o instanceof Question) {
			Question other = (Question) o;
			otherId = other.getId();
		}

		if (id == null && otherId == null) {
			return 0;
		} else if (id == null) {
			return -1;
		} else if (otherId == null) {
			return +1;
		}
		return id.compareTo(otherId);
	}

}
