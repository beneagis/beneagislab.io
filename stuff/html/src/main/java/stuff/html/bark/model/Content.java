package stuff.html.bark.model;

import java.util.ArrayList;
import java.util.List;

public class Content {
	private String[] titles;
	private String hash;
	private String index;
	private String node;
	private String html;
	private String translatedHtml;
	private String text;
	private String translatedText;

	private List<Content> children;

	public Content(String[] titles, String hash, String index, String node, String html, String translatedHtml,
			String text, String translatedText) {
		super();
		this.titles = titles;
		this.hash = hash;
		this.index = index;
		this.node = node;
		this.html = html;
		this.translatedHtml = translatedHtml;
		this.text = text;
		this.translatedText = translatedText;
		this.children = new ArrayList<Content>();
	}

	public String[] getTitles() {
		return titles;
	}

	public void setTitles(String[] titles) {
		this.titles = titles;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public String getTranslatedHtml() {
		return translatedHtml;
	}

	public void setTranslatedHtml(String translatedHtml) {
		this.translatedHtml = translatedHtml;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTranslatedText() {
		return translatedText;
	}

	public void setTranslatedText(String translatedText) {
		this.translatedText = translatedText;
	}

	public List<Content> getChildren() {
		return children;
	}

}
