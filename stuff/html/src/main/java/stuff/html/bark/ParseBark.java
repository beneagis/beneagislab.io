package stuff.html.bark;

import static stuff.util.CsvUtil.h;
import static stuff.util.CsvUtil.i;
import static stuff.util.CsvUtil.t;
import static stuff.util.FileUtil.s;
import static stuff.util.TranslateUtil.tr;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import com.google.common.collect.Sets;

import stuff.util.CsvUtil;
import stuff.util.FileUtil;
import stuff.util.TranslateUtil;

public class ParseBark {
	private static final String IDENT = " ";
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyMMddHHmmssSSS");

	private static Set<String> h3discard = Sets.newHashSet("AOL", "AT&T", "Charter", "Cox", "Dish", "Google Fi",
			"Google Fiber", "Spectrum", "Sprint", "T-Mobile", "Verizon", "Xfinity / Comcast");

	private static Set<String> s2discard = Sets.newHashSet("How to Monitor Content",
			"Bark - The Internet Safety Solution", "Want to learn more?");

	private static String date() {
		return DATE_FORMAT.format(Calendar.getInstance().getTime());
	}

	private static final String EXECUTION_DATE = date();

	private static void map(Map<String, Map<String, Map<MultiKey<String>, String>>> mmap, String key, String[] keys,
			String value) {
		Map<String, Map<MultiKey<String>, String>> map = mmap.get(key);
		if (map == null) {
			map = new TreeMap<String, Map<MultiKey<String>, String>>();
			mmap.put(key, map);
		}

		String hkeys = StringUtils.join(keys[3], ',', keys[5]);
		MultiKey<String> ckeys = new MultiKey<String>(keys);

		Map<MultiKey<String>, String> hmap = map.get(hkeys);
		if (hmap == null) {
			hmap = new LinkedHashMap<MultiKey<String>, String>();
			map.put(hkeys, hmap);
		}

		hmap.put(ckeys, value);
		if ("header".equals(key)) {
			map(mmap, "content", keys, value);
		}
	}

	public static void main(String[] args) throws IOException {
		Map<String, Map<String, Map<MultiKey<String>, String>>> mmap = new LinkedHashMap<String, Map<String, Map<MultiKey<String>, String>>>();
		try {

			CsvUtil scenariolog = new CsvUtil("bark/log/scenariolog-" + EXECUTION_DATE + ".csv", true, false, //
					"hash", // 0
					"node", // 0
					"index", // 0
					"scenario", // 1
					"h2", // 2
					"th2", // 3
					"h3", // 4
					"th3", // 5
					// "c1", // 6
					// "s1", // 7
					// "ts1", // 8
					// "c2", // 9
					"s2", // 10
					"ts2", // 11
					// "c3", // 12
					"s3", // 13
					"ts3", // 14
					// "c4", // 15
					// "s4", // 16
					// "ts4", // 17
					// "c5", // 18
					// "s5", // 19
					// "ts5", // 20
					// "c6", // 21
					// "s6", // 22
					// "ts6", // 23
					// "ch", // 24
					"html", // 25
					"thtml", // 26
					"name", // 28
					"attributes", // 29
					"text", // 30
					"translatedText" // 31
			);

			File root = new File("bark/html/raw/");
			for (File file : root.listFiles((dir, name) -> name.toLowerCase().endsWith(".html"))) {
				String scenario = StringUtils.substringBefore(file.getName(), ".");
				log(scenariolog, new Object[] { //
						h(scenario), // 0
						scenario // 1
				});

				Document doc = Jsoup.parse(file, "UTF-8", "https://bark-o-matic.com/results/0");
				Elements elements = doc.select(".container > .row > div > *");

				String h2 = "";
				String th2 = "";
				for (Element element : elements) {
					if ("h2".equalsIgnoreCase(element.nodeName())) {
						h2 = t(element.text());
						th2 = tr(h2);

						// map(mmap, "header", new String[] { //
						// "", h2, // 2
						// th2 // 3
						// }, h(h2)// 0
						// );

						log(scenariolog, new Object[] { //
								h(h2), // 0
								"", //
								"", //
								scenario, // 1
								h2, // 2
								th2 // 3
						});
					} else {
						String h3 = t(element.select("h3.mb-0").text());
						String th3 = tr(h3);

						if (!h3discard.contains(h3)) {

							// map(mmap, "header", new String[] { //
							// "", h2, // 2
							// th2, // 3
							// h3, // 4
							// th3, // 5
							// }, h(h3)// 0
							// );

							log(scenariolog, new Object[] { //
									h(h3), // 0
									"", //
									"", //
									scenario, // 1
									h2, // 2
									th2, // 2
									h3, // 3
									th3 // 3
							});

							for (Element content : element.select("div.trix-content")) {
								String s1 = "";
								String s2 = "";
								String s3 = "";
								String s4 = "";
								String s5 = "";
								String s6 = "";
								String html = "";

								String ts1 = "";
								String ts2 = "";
								String ts3 = "";
								String ts4 = "";
								String ts5 = "";
								String ts6 = "";
								String thtml = "";

								int c1 = 0;
								int c2 = 0;
								int c3 = 0;
								int c4 = 0;
								int c5 = 0;
								int c6 = 0;
								int ch = 0;

								String node = "";

								for (Element s : content.children()) {
									boolean skip = false;
									String h = "";

									if ("h1".equalsIgnoreCase(s.nodeName())) {
										throw new IllegalStateException(s.outerHtml());
									} else if ("h2".equalsIgnoreCase(s.nodeName())) {
										s2 = t(s.text());
										s3 = "";
										s4 = "";
										s5 = "";
										s6 = "";
										html = "";

										ts2 = tr(s2);
										ts3 = "";
										ts4 = "";
										ts5 = "";
										ts6 = "";
										thtml = "";

										++c2;
										c3 = 0;
										c4 = 0;
										c5 = 0;
										c6 = 0;
										ch = 0;

										h = h(s2);
										node = "h4";

										skip = skip || s2discard.contains(s2);

										if (!skip) {
											map(mmap, "header", new String[] { //
													node, // 0
													i(c2, c3, ch), //
													h2, // 2
													th2, // 3
													h3, // 4
													th3, // 5
													// t(c1), // 6
													// s1, // 7
													// ts1, // 8
													// t(c2), // 9
													s2, // 10
													ts2, // 11
													// t(c3), // 12
													s3, // 13
													ts3, // 14
													// t(c4), // 15
													// s4, // 16
													// ts4, // 17
													// t(c5), // 18
													// s5, // 19
													// ts5, // 20
													// t(c6), // 21
													// s6, // 22
													// ts6, // 23
													// t(ch), // 24
													html, // 25
													thtml, // 26
											}, h// 0
											);
										}
									} else if ("h3".equalsIgnoreCase(s.nodeName())) {
										s3 = t(s.text());
										s4 = "";
										s5 = "";
										s6 = "";
										html = "";

										ts3 = tr(s3);
										ts4 = "";
										ts5 = "";
										ts6 = "";
										thtml = "";

										++c3;
										c4 = 0;
										c5 = 0;
										c6 = 0;
										ch = 0;

										h = h(s3);
										node = "h5";

										skip = skip || s2discard.contains(s2);

										if (!skip) {
											map(mmap, "header", new String[] { //
													node, // 0
													i(c2, c3, ch), //
													h2, // 2
													th2, // 3
													h3, // 4
													th3, // 5
													// t(c1), // 6
													// s1, // 7
													// ts1, // 8
													// t(c2), // 9
													s2, // 10
													ts2, // 11
													// t(c3), // 12
													s3, // 13
													ts3, // 14
													// t(c4), // 15
													// s4, // 16
													// ts4, // 17
													// t(c5), // 18
													// s5, // 19
													// ts5, // 20
													// t(c6), // 21
													// s6, // 22
													// ts6, // 23
													// t(ch), // 24
													html, // 25
													thtml, // 26
											}, h// 0
											);
										}
									} else if ("h4".equalsIgnoreCase(s.nodeName())) {
										throw new IllegalStateException(s.outerHtml());
									} else if ("h5".equalsIgnoreCase(s.nodeName())) {
										throw new IllegalStateException(s.outerHtml());
									} else if ("h6".equalsIgnoreCase(s.nodeName())) {
										throw new IllegalStateException(s.outerHtml());
									} else {

										if ("div".equalsIgnoreCase(s.nodeName())) {
											node = "p";
										} else {
											node = s.nodeName();
										}
										html = s.html();

										html = t(s.html());

										if (StringUtils.isBlank(html) || html.matches("^(<br>|&nbsp;|[ ])+$")) {
											skip = true;
											// System.err.println(html);

											html = "";
											h = "";
											thtml = "";
										} else {
											++ch;
											h = h(html);

											if (StringUtils.isNotBlank(html)) {
												thtml = tr(html, "html");
											}

										}
									}

									skip = skip || s2discard.contains(s2);

									if (!skip) {
										String index = i(/* c1, */
												c2, c3,
												/* c4, c5, c6, */
												ch);

										String[] values = new String[] { //
												h, // 0
												node, // 0
												index, // 0
												scenario, // 1
												h2, // 2
												th2, // 3
												h3, // 4
												th3, // 5
												// t(c1), // 6
												// s1, // 7
												// ts1, // 8
												// t(c2), // 9
												s2, // 10
												ts2, // 11
												// t(c3), // 12
												s3, // 13
												ts3, // 14
												// t(c4), // 15
												// s4, // 16
												// ts4, // 17
												// t(c5), // 18
												// s5, // 19
												// ts5, // 20
												// t(c6), // 21
												// s6, // 22
												// ts6, // 23
												// t(ch), // 24
												html, // 25
												thtml, // 26

										};

										log(scenariolog, values);

										// System.out.println("" + index);
										// visit(contentlog, index, s, values);

										map(mmap, "content", new String[] { //
												node, // 0
												index, // 1
												h2, // 2
												th2, // 3
												h3, // 4
												th3, // 5
												// t(c1), // 6
												// s1, // 7
												// ts1, // 8
												// t(c2), // 9
												s2, // 10
												ts2, // 11
												// t(c3), // 12
												s3, // 13
												ts3, // 14
												// t(c4), // 15
												// s4, // 16
												// ts4, // 17
												// t(c5), // 18
												// s5, // 19
												// ts5, // 20
												// t(c6), // 21
												// s6, // 22
												// ts6, // 23
												// t(ch), // 24
												html, // 25
												thtml, // 26
										}, h// 0
										);
									}
								}

							}
						}
					}
				}
			}
			TranslateUtil.done(true);
		} catch (Exception e) {
			TranslateUtil.done(true);
		} finally {
			logMap(mmap);
		}
	}

	private static void logMap(Map<String, Map<String, Map<MultiKey<String>, String>>> mmap) throws IOException {
		StringBuilder allEn = new StringBuilder();
		StringBuilder allPt = new StringBuilder();

		for (Entry<String, Map<String, Map<MultiKey<String>, String>>> entry : mmap.entrySet()) {
			String key = entry.getKey();
			CsvUtil log = new CsvUtil("bark/log/" + key + "log-" + EXECUTION_DATE + ".csv", true, false, //
					"hash", // 0
					"node", // 1
					"index", // 2
					"h2", // 3
					"th2", // 4
					"h3", // 5
					"th3", // 6
					// "c1", //
					// "s1", //
					// "ts1", //
					// "c2", //
					"s2", // 7
					"ts2", // 8
					// "c3", //
					"s3", // 9
					"ts3", // 10
					// "c4", //
					// "s4", //
					// "ts4", //
					// "c5", //
					// "s5", //
					// "ts5", //
					// "c6", //
					// "s6", //
					// "ts6", //
					// "ch", //
					"html", // 11
					"thtml", // 12
					"name", // 13
					"attributes", // 14
					"text", // 15
					"translatedText" // 16
			);

			for (Entry<String, Map<MultiKey<String>, String>> entry1 : entry.getValue().entrySet()) {
				Map<MultiKey<String>, String> map = entry1.getValue();

				StringBuilder en = new StringBuilder();
				StringBuilder pt = new StringBuilder();

				String title = entry1.getKey().replace(",", " - ");

				en.append("<h3>" + title + "</h3>\n");
				pt.append("<h3>" + title + "</h3>\n");

				for (Entry<MultiKey<String>, String> entry2 : map.entrySet()) {
					String[] keys = entry2.getKey().getKeys();
					String[] values = ArrayUtils.addAll(new String[] { entry2.getValue() }, keys);
					log(log, values);

					String tag = "  <" + values[1] + " id=\"" + values[2] + "\">";
					String endTag = "</" + values[1] + ">\n";

					en.append(tag);
					pt.append(tag);
					if (!StringUtils.isAllBlank(values[11], values[12])) {
						en.append(values[11]);
						pt.append(values[12]);
					} else if (!StringUtils.isAllBlank(values[9], values[10])) {
						en.append(values[9]);
						pt.append(values[10]);
					} else if (!StringUtils.isAllBlank(values[7], values[8])) {
						en.append(values[7]);
						pt.append(values[8]);
					}
					en.append(endTag);
					pt.append(endTag);
				}
				if ("content".equals(key)) {

					allEn.append(en);
					allPt.append(pt);

					String file = s(entry1.getKey().replace(',', '-'));
					FileUtil.write("bark/html/exported/" + file + "_en.html", en.toString());
					FileUtil.write("bark/html/exported/" + file + "_pt.html", pt.toString());
				}
			}
		}

		String tag = "<!DOCTYPE html>\n" + "<html>\n" + "<head>\n" + "  <meta charset=\"utf-8\">\n"
				+ "  <title></title>\n" + "  <meta name=\"description\" content=\"\">\n"
				+ "  <meta name=\"author\" content=\"\">\n"
				+ "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
				+ "  <link href=\"//fonts.googleapis.com/css?family=Raleway:400,300,600\" rel=\"stylesheet\" type=\"text/css\">\n"
				+ "  <link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css\">\n"
				+ "  <link rel=\"stylesheet\" href=\"//cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.css\">\n"
				+ "  <link rel=\"icon\" type=\"image/png\" href=\"images/favicon.png\">\n"

				+ "<style>" + "h1, h2, h3, h4, h5, h6 {font-weight: bold;border-bottom:1px solid #aaa;}" + "</style>"
				+ "</head>\n" + "<body>\n" + "<div class=\"container\">\n";

		String endTag = "</div>\n" + "</body>\n" + "</html>\n";

		FileUtil.write("bark/html/exported/_all_en.html", tag + allEn + endTag);
		FileUtil.write("bark/html/exported/_all_pt.html", tag + allPt + endTag);

	}

	private static void log(CsvUtil log, Object[] values) throws IOException {
		for (int i = 0; i < values.length; i++) {
			values[i] = t(values[i]);
		}
		log.add(Arrays.asList(values));
		log.flush();
	}

	private static void visit(CsvUtil contentlog, String index, Node node, Object[] values) throws IOException {
		visit(contentlog, 0, index, node, values);
	}

	private static void visit(CsvUtil contentlog, int level, String index, Node node, Object[] values)
			throws IOException {
		String name = node.nodeName();
		boolean skip = false;
		boolean stop = false;
		String type = "none";
		String text = "";
		String className = "";
		String attributes = "";

		if (node instanceof DocumentType) {
			type = "doctype";
		} else if (node instanceof Element) {
			type = "element";
			Element el = (Element) node;
			// text = el.text();
			className = el.className();

			attributes = el.attributes().toString().trim();
		} else if (node instanceof TextNode) {
			type = "text";

			TextNode txt = (TextNode) node;
			text = txt.text();
			skip = skip || StringUtils.isBlank(text);
		} else {
			throw new IllegalStateException(node.outerHtml());
		}

		if (!skip) {

			String translatedText = "";
			if (StringUtils.isNotBlank(text)) {
				text = t(text);
				translatedText = tr(text);
			}

			Object[] v = Arrays.copyOf(values, values.length + 4);
			v[v.length - 5] = index;
			v[v.length - 4] = StringUtils.repeat(IDENT, level) + name;
			v[v.length - 3] = attributes;
			v[v.length - 2] = text;
			v[v.length - 1] = translatedText;

			log(contentlog, v);

			// System.out.println(StringUtils.repeat(IDENT, level) + name + " " + index + "
			// " + attributes + text);
		}

		if (!stop) {
			int i = 0;
			for (Node child : node.childNodes()) {
				visit(contentlog, level + 1, (index + "." + ++i), child, values);
			}
		}
	}
}
