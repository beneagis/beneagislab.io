package stuff;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

public class App {
	private static final String IDENT = " ";

	public static void main(String[] args) throws IOException {

		File input = new File("bark/html/raw/All.html");
		System.out.println(input.getAbsolutePath());

		Document doc = Jsoup.parse(input, "UTF-8", "https://bark-o-matic.com/results/0");
		Elements elements = doc.select(".container > .row > div > *");
		for (Element element : elements) {
			if ("h2".equalsIgnoreCase(element.nodeName())) {
				System.out.println();
				System.out.println(" " + element.text());

			} else {
				System.out.println("  " + element.select("h3.mb-0").text());
				for (Element content : element.select("div.trix-content")) {
					// System.out.println(content.outerHtml().replaceAll("[\n\t ]+", " ").replace(">
					// <", "><"));
					// visit(3, content);
				}

			}
		}
		// elements.forEach(App::visit);

		System.out.println("-");
	}

	private static void visit(Node node) {
		visit(0, node);
	}

	private static void visit(int level, Node node) {
		String name = node.nodeName();
		boolean skip = false;
		boolean stop = false;
		String type = "none";
		String text = "";
		String className = "";

		if (node instanceof DocumentType) {
			type = "doctype";
		} else if (node instanceof Element) {
			type = "element";

			Element el = (Element) node;
			// text = el.text();
			className = el.className();

			// skip = skip || "container".equals(className);
			// skip = skip || "row".equals(className);
			// skip = skip || "d-block pb-3".equals(className);
			// skip = skip || "pull-right".equals(className);
			// skip = skip || "collapse".equals(className);
			// skip = skip || "p-3 p-md-5".equals(className);
			// skip = skip || "col mb-5".equals(className);

			// skip = skip || "div".equals(name);
			// skip = skip || "br".equals(name);
			// stop = stop || "br".equals(name);
			// skip = skip || "ol".equals(name);
			// stop = stop || "ol".equals(name);
			// skip = skip || "ul".equals(name);
			// stop = stop || "ul".equals(name);
			// skip = skip || "li".equals(name);
			// stop = stop || "li".equals(name);

		} else if (node instanceof TextNode) {
			type = "text";

			TextNode txt = (TextNode) node;
			text = txt.text();
			skip = skip || StringUtils.isBlank(text);
		} else {
		}

		if (!skip) {
			System.out.println(StringUtils.repeat(IDENT, level) + name
					+ (StringUtils.isNotBlank(className) ? " [" + className + "] " : "") + " (" + type + ") =>" + text);
		}

		if (!stop) {
			for (Node child : node.childNodes()) {
				visit(level + 1, child);
			}
		}
	}

}
