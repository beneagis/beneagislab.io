package com.lazerycode.selenium.filedownloader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Set;

import javax.net.ssl.SSLException;

import msfranca.stuff.downloader.beehive.Download;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.DateUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FileDownloader {

	private WebDriver driver;
	private String localDir = System.getProperty("java.io.tmpdir");
	private boolean mimicWebDriverCookieState = true;
	private int statusCode = 0;

	public FileDownloader(WebDriver driverObject) {
		this.driver = driverObject;
	}

	public String localDownloadPath() {
		return this.localDir;
	}

	public void localDownloadPath(String filePath) {
		this.localDir = filePath;
	}

	public Download downloadFile(WebElement element, boolean redirect)
			throws Exception {
		return download(element, "href", redirect);
	}

	public Download downloadImage(WebElement element, boolean redirect)
			throws Exception {
		return download(element, "src", redirect);
	}

	public int getHTTPStatusOfLastDownloadAttempt() {
		return this.statusCode;
	}

	public void mimicWebDriverCookieState(boolean value) {
		this.mimicWebDriverCookieState = value;
	}

	private BasicCookieStore mimicCookieState(Set<Cookie> seleniumCookieSet) {
		BasicCookieStore mimicWebDriverCookieStore = new BasicCookieStore();
		for (Cookie seleniumCookie : seleniumCookieSet) {
			BasicClientCookie duplicateCookie = new BasicClientCookie(
					seleniumCookie.getName(), seleniumCookie.getValue());
			duplicateCookie.setDomain(seleniumCookie.getDomain());
			duplicateCookie.setSecure(seleniumCookie.isSecure());
			duplicateCookie.setExpiryDate(seleniumCookie.getExpiry());
			duplicateCookie.setPath(seleniumCookie.getPath());
			mimicWebDriverCookieStore.addCookie(duplicateCookie);
		}

		return mimicWebDriverCookieStore;
	}

	private Download download(WebElement element, String attribute,
			boolean redirect) throws IOException, NullPointerException,
			URISyntaxException {
		String fileToDownloadLocation = element.getAttribute(attribute);
		return download(fileToDownloadLocation, redirect, null);
	}

	public Download download(String remotePath, boolean redirect,
			String filePrefix) throws IOException, NullPointerException,
			URISyntaxException {

		if (remotePath == null || remotePath.trim().equals("")) {
			throw new NullPointerException(
					"The element you have specified does not link to anything!");
		}

		File dir = new File(this.localDir);

		if (!dir.exists() || !dir.isDirectory()) {
			throw new IllegalArgumentException("Directory does not exist: "
					+ this.localDir);
		}

		LaxRedirectStrategy redirectStrategy = new LaxRedirectStrategy();
		HttpClient client = HttpClients.custom()
				.setRetryHandler(new RetryHandler())
				.setRedirectStrategy(redirectStrategy).build();

		BasicHttpContext localContext = new BasicHttpContext();

		if (this.mimicWebDriverCookieState) {
			localContext.setAttribute(HttpClientContext.COOKIE_STORE,
					mimicCookieState(this.driver.manage().getCookies()));
		}

		URL fileToDownload = new URL(remotePath);
		HttpGet httpget = new HttpGet(fileToDownload.toURI());

		HttpResponse response = client.execute(httpget, localContext);
		this.statusCode = response.getStatusLine().getStatusCode();

		String status;
		long remoteFileLength;
		File localFile = null;
		String localPath = null;
		try {
			Date lastModified = DateUtils.parseDate(response.getLastHeader(
					"Last-Modified").getValue());

			HttpEntity entity = response.getEntity();
			remoteFileLength = entity.getContentLength();

			// "SKIP : "
			// "DOING: "
			// "OK   : "
			// "       "

			String filePath = null;
			if (redirect) {
				HttpUriRequest request = ((HttpUriRequest) localContext
						.getAttribute("http.request"));
				if (request != null) {
					filePath = request.getURI().getPath();
				}
			}

			if (filePath == null) {
				filePath = fileToDownload.getFile();
			}
			if (filePrefix != null) {
				filePath = filePrefix + "/"
						+ StringUtils.substringAfterLast(filePath, "/");
			}
			localFile = new File(this.localDir
					+ URLDecoder.decode(filePath, "UTF-8").replaceFirst(
							"/|\\\\", ""));

			localPath = localFile.getAbsolutePath();
			long localFileLength = localFile.length();

			if (localFile.exists() && localFileLength != 0
					&& localFileLength == remoteFileLength) {
				System.out.println("  OK   : "
						+ FileUtils.byteCountToDisplaySize(remoteFileLength)
						+ " - " + remotePath + "\n  " + "       "
						+ FileUtils.byteCountToDisplaySize(localFileLength)
						+ " - " + localPath);
				status = "OK";
			} else if (remoteFileLength < 0
			// || remoteFileLength > 1024 * 1024 * 500
			// || localPath.contains("SOABPM12c_Migration.7z")
			// || localPath.contains("IotServerVM-12c-OOW2014HOL.ova")
			// || localPath.contains("CAF-12.2.1.zip.")
			// || localPath.contains("/WLS12cWorkshopPTS/WLS12cWorkshop/kits/")
			) {
				System.out.println("  SKIP : "
						+ FileUtils.byteCountToDisplaySize(remoteFileLength)
						+ " - " + remotePath + "\n  " + "       "
						+ FileUtils.byteCountToDisplaySize(localFileLength)
						+ " - " + localPath);
				status = "SKIP";
			} else {
				if (!localFile.canWrite()) {
					localFile.setWritable(true);
				}
				System.out.println("  DOING: "
						+ FileUtils.byteCountToDisplaySize(remoteFileLength)
						+ " - " + remotePath + "\n  " + "       "
						+ FileUtils.byteCountToDisplaySize(localFileLength)
						+ " - " + localPath);

				InputStream in = null;
				try {
					in = entity.getContent();
					FileUtils.copyInputStreamToFile(in, localFile);
					status = "DONE";
				} catch (Exception e) {
					status = "ERROR";
					throw e;
				} finally {
					if (in != null) {
						in.close();
					}
				}
			}

			localFile.setLastModified(lastModified.getTime());
		} catch (Exception e) {
			if (localFile != null) {
				localFile.delete();
			}
			throw new RuntimeException(e);
		}

		return new Download(remotePath, localPath, remoteFileLength, status);
	}

	static class RetryHandler implements HttpRequestRetryHandler {

		public boolean retryRequest(IOException exception, int executionCount,
				HttpContext context) {
			if (executionCount >= 5) {
				// Do not retry if over max retry count
				return false;
			}
			if (exception instanceof InterruptedIOException) {
				// Timeout
				return false;
			}
			if (exception instanceof UnknownHostException) {
				// Unknown host
				return false;
			}
			if (exception instanceof ConnectTimeoutException) {
				// Connection refused
				return false;
			}
			if (exception instanceof SSLException) {
				// SSL handshake exception
				return false;
			}
			HttpClientContext clientContext = HttpClientContext.adapt(context);
			HttpRequest request = clientContext.getRequest();
			boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
			if (idempotent) {
				// Retry if the request is considered idempotent
				return true;
			}
			return false;
		}
	}

}