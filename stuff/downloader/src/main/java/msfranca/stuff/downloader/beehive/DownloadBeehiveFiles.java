package msfranca.stuff.downloader.beehive;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DownloadBeehiveFiles {

	private final static String[] URLs = {
	// DONE:

	// DOING:
	"https://beehiveonline.oracle.com/content/dav/Oracle/Oracle%20PAAS%20-%20Integration%20Cloud%20Workshop/",
	// TODO:
	// "https://beehiveonline.oracle.com/content/dav/",

	};

	public static void main(String[] args) throws Exception {

		// new DownloadThread(URLs).run();

		DownloadThread.init(URLs);

		ExecutorService executor = Executors.newFixedThreadPool(1000);
		executor.execute(new DownloadThread());
		Thread.sleep(30000);

		for (int i = 1; i <= 3; i++) {
			executor.execute(new DownloadThread());
		}
		executor.shutdown();
	}
}
