package msfranca.stuff.downloader.beehive;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import com.lazerycode.selenium.filedownloader.FileDownloader;

public class DownloadTMForumFiles {
	private final static String LOCAL_PATH = "/Volumes/Franca128GB/Beehive/";

	private final static String USER_FIELD_ID = "username";
	private final static String PASSWORD_FIELD_ID = "password";

	private final static String LOGIN_URL = "https://www.tmforum.org/my-account/";

	private final static String URL = "https://www.tmforum.org/resources/";

	private final static String USER_VALUE = "aron.borges@tcs.com";
	private final static String PASSWORD_VALUE = "Ceni&baby24";

	private final static LinkedList<Download> todo = new LinkedList<Download>();
	private final static Set<String> done = new HashSet<String>();
	private final static Set<Download> downloads = new TreeSet<Download>();

	private static void auth(WebDriver driver) {
		if (driver.getTitle().contains("My Account")) {
			WebElement element;
			element = driver.findElement(By.id(USER_FIELD_ID));
			element.sendKeys(USER_VALUE);
			element = driver.findElement(By.id(PASSWORD_FIELD_ID));
			element.sendKeys(PASSWORD_VALUE);
			element.submit();
		}
	}

	private static String getValue(String text) {
		String[] parts = text.split(": ", 2);
		if (parts.length > 1) {
			return parts[1];
		}
		return null;
	}

	public static void main(String[] args) throws Exception {

		FirefoxProfile profile = new FirefoxProfile();

		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting",
				false);
		profile.setPreference("browser.download.dir",
				"/Users/marciofranca/Downloads/selenium/");
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
				"text/csv");

		WebDriver driver = new FirefoxDriver(profile);
		FileDownloader downloader = new FileDownloader(driver);
		downloader.localDownloadPath(LOCAL_PATH);

		driver.get(LOGIN_URL);
		auth(driver);

		for (int i = 1; i <= 74; i++) {
			todo.push(new Download(URL + i, 0, 1, 0, 1));
		}

		Download download;
		String url;

		while (!todo.isEmpty()) {
			download = todo.pop();
			url = download.href;

			List<? extends WebElement> items;

			done.add(url);
			System.out.println("DOING: " + url);
			driver.get(url);
			auth(driver);

			WebElement element = driver.findElement(By.className("grid-view"));
			if (element != null) {
				element.click();
			}

			items = driver.findElements(By.cssSelector("li.product"));

			for (WebElement item : items) {

				WebElement title = item.findElement(By
						.cssSelector("a.product_title_main"));

				WebElement version = item.findElement(By
						.cssSelector("p.product_title_attr_version"));

				WebElement standard = item.findElement(By
						.cssSelector("p.product_title_attr_standard"));

				System.out.println("  Title: " + title.getText());
				System.out.println("    URL: " + title.getAttribute("href"));
				System.out.println("    " + getValue(version.getText()));
				System.out.println("    " + getValue(standard.getText()));

				// if (item instanceof WebElement) {
				// WebElement link = (WebElement) item;
				// href = link.getAttribute("href");
				// text = link.getText();
				//
				// } else {
				// href = String.valueOf(item);
				// }
				//
				// if (href != null
				// &&
				// !"https://beehiveonline.oracle.com/content/dav?action=logout"
				// .equalsIgnoreCase(href)
				// && !"Parent Directory".equalsIgnoreCase(text)
				// &&
				// href.startsWith("https://beehiveonline.oracle.com/content/"))
				// {
				// if (href.endsWith("/")) {
				// if (!done.contains(href)) {
				// System.out.println("  TODO: " + href);
				// todo.add(new Download(href));
				// }
				// } else {
				// System.out.println("  DOING: " + href);
				// // downloads.add(downloader.download(href));
				// }
				// }
			}
		}

		int count = 0;
		for (Download d : downloads) {
			count++;
			if (!"OK".equalsIgnoreCase(d.status)
					&& !"DONE".equalsIgnoreCase(d.status)) {
				System.out.println(d.status + " " + (count) + "/"
						+ downloads.size() + ": " + d.href);
			}
		}

		/*
		 * (new WebDriverWait(driver, 10)).until(new
		 * ExpectedCondition<Boolean>() { public Boolean apply(WebDriver d) {
		 * return d.getTitle().toLowerCase().startsWith("cheese!"); } });
		 */

		// driver.quit();
	}
}
