package msfranca.stuff.downloader.beehive;

public class Download implements Comparable<Download> {
	public String href;
	public String path;
	public long length;
	public int index;
	public int count;
	public float start;
	public float end;
	public String status;

	public Download(String href) {
		super();
		this.href = href;
	}

	public Download(String href, int index, int count, float start, float end) {
		super();
		this.href = href;
		this.index = index;
		this.count = count;
		this.start = start;
		this.end = end;
	}

	public Download(String href, String path, long length, String status) {
		super();
		this.href = href;
		this.path = path;
		this.length = length;
		this.status = status;
	}

	public int compareTo(Download o) {
		int result = this.status.compareTo(o.status);
		if (result == 0) {
			result = this.href.compareTo(o.href);
		}
		return result;
	}
}