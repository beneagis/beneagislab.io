package msfranca.stuff.downloader.beehive;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import com.lazerycode.selenium.filedownloader.FileDownloader;

public class DownloadThread implements Runnable {

	private final static String LOCAL_PATH = "/Volumes/Franca128GB/Beehive/";
	private final static String USER_FIELD_NAME = "ssousername";
	private final static String PASSWORD_FIELD_NAME = "password";
	// private final static String LOGIN_URL =
	// "https://beehiveonline.oracle.com/content/dav";
	// private final static String USER_VALUE = "solange.melo@sysmap.com.br";
	// private final static String PASSWORD_VALUE = "Sol#sysmap01";
	private final static String USER_VALUE = "marcio.franca@sysmap.com.br";
	private final static String PASSWORD_VALUE = "Jan01Jan";

	private String url;

	private static Stack<Download> todo = new Stack<Download>();
	private static Set<String> done = new HashSet<String>();
	private static Set<Download> downloads = new TreeSet<Download>();

	public static void init(String[] urls) {
		for (String url : urls) {
			todo.push(new Download(url, 0, 1, 0, 1));
		}
	}

	public void run() {
		try {

			FirefoxProfile profile = new FirefoxProfile();

			profile.setPreference("browser.download.folderList", 2);
			profile.setPreference("browser.download.manager.showWhenStarting",
					false);
			profile.setPreference("browser.download.dir",
					"/Users/marciofranca/Downloads/selenium/");
			profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
					"text/csv");

			WebDriver driver = new FirefoxDriver(profile);
			FileDownloader downloader = new FileDownloader(driver);
			downloader.localDownloadPath(LOCAL_PATH);

			String url;
			Download download;

			while (!todo.isEmpty()) {
				download = todo.pop();
				url = download.href;
				done.add(url);

				List<? extends Object> links;

				if (url.endsWith("/")) {
					// System.out.println("DOING: " + url);
					driver.get(url);
					if (driver.getTitle().contains("Single Sign On - Login")) {
						WebElement element;
						element = driver.findElement(By.name(USER_FIELD_NAME));
						element.sendKeys(USER_VALUE);
						element = driver.findElement(By
								.name(PASSWORD_FIELD_NAME));
						element.sendKeys(PASSWORD_VALUE);
						element.submit();
					}

					links = driver.findElements(By.tagName("a"));
				} else {
					driver.get(url + "/..");
					if (driver.getTitle().contains("Single Sign On - Login")) {
						WebElement element;
						element = driver.findElement(By.name(USER_FIELD_NAME));
						element.sendKeys(USER_VALUE);
						element = driver.findElement(By
								.name(PASSWORD_FIELD_NAME));
						element.sendKeys(PASSWORD_VALUE);
						element.submit();
					}

					links = Arrays.asList(url);
				}

				for (Object item : links) {
					String href;
					String text = null;
					if (item instanceof WebElement) {
						WebElement link = (WebElement) item;
						href = link.getAttribute("href");
						text = link.getText();

					} else {
						href = String.valueOf(item);
					}

					if (href != null
							// TODO: REMOVE
							&& !href.endsWith("IotServerVM-12c-OOW2014HOL.ova")
							&& !href.endsWith("IotServerVM-12c-OOW2014HOL_error.ova")
							&& !href.endsWith("OER_TrainingVBox.zip")
							&& !"https://beehiveonline.oracle.com/content/dav?action=logout"
									.equalsIgnoreCase(href)
							&& !"Parent Directory".equalsIgnoreCase(text)
							&& href.startsWith("https://beehiveonline.oracle.com/content/")) {
						if (href.endsWith("/")) {
							if (!done.contains(href)) {
								// System.out.println("TODO : " + href);
								todo.add(new Download(href));
							}
						} else {
							try {
								// System.out.println("DOING: " + href);
								downloads.add(downloader.download(href, false,
										null));
							} catch (Exception e) {
								new RuntimeException(href, e).printStackTrace();
								todo.add(new Download(href));
							}

						}
					}
				}
			}

			int count = 0;
			for (Download d : downloads) {
				count++;
				if (!"OK".equalsIgnoreCase(d.status)) {
					System.out.println(d.status + " " + (count) + "/"
							+ downloads.size() + " "
							+ FileUtils.byteCountToDisplaySize(d.length) + ": "
							+ d.href);
				}
			}

			driver.quit();

		} catch (Exception e) {
			throw new RuntimeException(url, e);
		}
	}
}